/**
 * Created by jamesspence on 8/4/15.
 */

/**
 * Adds an apostrophe at the end of a name.
 * @param name
 * @returns string
 */
exports.apostrophize = function (name) {
	if (name[name.length - 1] == 's') {
		name+= '\'';
	} else {
		name+= '\'s';
	}
	return name;
};