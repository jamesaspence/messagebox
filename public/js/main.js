/**
 * Created by jamesspence on 8/4/15.
 */
exports.initializeCKEditor = function(elementId) {
	$('#' + elementId).ckeditor();
};