var express = require('express');
var app = express();
var functions = require('./assets/js/functions');
var main = require('./assets/js/main');

app.set('views', './views');
app.set('view engine', 'jade');
app.use(express.static('public'));

app.get('/', function(req, res, next) {
	app.locals.functions = functions;
	app.locals.mainJS = main;
	res.render('index', { name: 'James'});
	next();
});

app.listen(3000);

